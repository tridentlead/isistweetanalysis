import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by danie on 5/30/2017.
 */
public class CSVLoader implements DataLoader {
    public HashMap<User, ArrayList<Tweet>> load() {
        HashMap<User, ArrayList<Tweet>> map = new HashMap<User, ArrayList<Tweet>>();
        try {
            Reader reader = new FileReader("C:\\Users\\danie\\Downloads\\how-isis-uses-twitter\\tweets.csv");
            for (CSVRecord record: CSVFormat.DEFAULT.parse(reader)) {
                if (record.get(0).equals("name")) {
                    continue;
                }
                User user = new User(record.get(0), Integer.parseInt(record.get(4)));
                if (map.get(user) == null) {
                    map.put(user, new ArrayList<Tweet>());
                }
                ArrayList<Tweet> tweets = map.get(user);
                tweets.add(new Tweet(user, record.get(7)));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return map;
    }
}
