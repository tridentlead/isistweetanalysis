import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by danie on 5/30/2017.
 */
public class Driver {
    public static void main(String[] argv) {
        DataLoader loader = new CSVLoader();
        HashMap<User, ArrayList<Tweet>> data = loader.load();
        for (User user : data.keySet()) {
            Tweet tweet = data.get(user).get(0);
            if (tweet == null) {
                continue;
            }
        }
        System.out.println("The following Users have at-least 500 followers:");
        for (User user : Finding.findUsersWithAtLeastFollowers(500, data)) {
            System.out.println(user.getUserName());
        }
        System.out.println("The 10 most popular words in pro-ISIS tweets are:");
        String lastword = "";
        for (int i = 0; i < 10; i++){
            lastword = Finding.wordOccurrencesToMostOccurringWordLessThan(lastword, Counting.occurrencesToPercentageOccurrence(Counting.overallWordOccurrences(data)));
            System.out.println(lastword + " Frequency: " + Counting.occurrencesToPercentageOccurrence(Counting.overallWordOccurrences(data)).get(lastword));
        }
        System.out.println(Counting.overallWordOccurrences(data).size());
        System.out.println("Here are a few tweets with the word 'Death' in them");
        ArrayList<Tweet> death = Finding.findWithWord("Death", data);
        for (int i = 0; i < 3; i++) {
            System.out.println(death.get(i).getBody());
        }
        System.out.println("Here are a few tweets with the word 'Infidel' in them");
        ArrayList<Tweet> infidel = Finding.findWithWord("Infidel", data);
        for (int i = 0; i < 3; i++) {
            System.out.println(infidel.get(i).getBody());
        }
    }
}
