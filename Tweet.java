import org.apache.commons.lang.StringUtils;

/**
 * Created by danie on 5/30/2017.
 */
public class Tweet {
    private final User user;
    private final String body;

    /**
     * The constructor for the Tweet class, creates a Tweet tied to a given user and with a given string as it's body
     * @param user The user who posted the Tweet
     * @param body The body of the Tweet
     */
    public Tweet(User user, String body) {
        this.user = user;
        this.body = body;
    }

    /**
     * @return The user who posted the tweet
     */
    public User getUser() {
        return user;
    }

    /**
     * @return The body of the tweet
     */
    public String getBody() {
        return body;
    }

    /**
     * @return The body of the tweet as a string array, with punctuation removed
     */
    public String[] getBodyAsStringArray() {
        String[] words = body.split("\\s+");
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].replaceAll("[^\\w]", "");
        }
        return words;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tweet tweet = (Tweet) o;

        if (!user.equals(tweet.user)) return false;
        return body.equals(tweet.body);
    }

    @Override
    public int hashCode() {
        int result = user.hashCode();
        result = 31 * result + body.hashCode();
        return result;
    }
}
