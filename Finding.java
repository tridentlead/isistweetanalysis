import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by danie on 6/1/2017.
 */
public class Finding {
    /**
     * @param word The word to be found
     * @param dataset The loaded dataset
     * @return The complete set of tweets containing the specified word
     */
    public static ArrayList<Tweet> findWithWord(String word, HashMap<User, ArrayList<Tweet>> dataset) {
        ArrayList<Tweet> foundTweets = new ArrayList<Tweet>();
        for (User key : dataset.keySet()) {
            ArrayList<Tweet> tempTweets = dataset.get(key);
            if (tempTweets == null) { continue; }
            for (Tweet tweet : tempTweets) {
                if (tweet.getBody().contains(word)) { foundTweets.add(tweet); }
            }
        }
        return foundTweets;
    }

    /**
     * @param occurrences A map of words to the number of times they occur
     * @return The most commonly occurring word
     */
    public static String wordOccurrencesToMostOccurringWordLessThan(String lessThan, HashMap<String, Double> occurrences) {
        String key = " ";
        Double largest = 0.00;
        for (String subKey : occurrences.keySet()) {
            if (occurrences.get(subKey) > largest && occurrences.get(subKey) < (occurrences.get(lessThan) == null ? Double.MAX_VALUE : occurrences.get(lessThan)) && !StringUtils.isBlank(subKey)) {
                largest = occurrences.get(subKey);
                key = subKey;
            }
        }
        return key;
    }

    /**
     * @param followers The number of followers as the base number
     * @param dataset The loaded dataset
     * @return The list of Users with atleast the given number of followers
     */
    public static ArrayList<User> findUsersWithAtLeastFollowers(int followers, HashMap<User, ArrayList<Tweet>> dataset) {
        ArrayList<User> ret = new ArrayList<User>();
        for (User user : dataset.keySet()) {
            if (user.getFollowers() >= followers) {
                ret.add(user);
            }
        }
        return ret;
    }
}
