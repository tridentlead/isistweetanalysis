import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by danie on 5/30/2017.
 */
public interface DataLoader {
    /**
     * Loads dataset into a valid representation for analysis
     * @return A hashmap of the data
     */
    public HashMap<User, ArrayList<Tweet>> load();
}
