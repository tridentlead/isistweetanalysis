/**
 * Created by danie on 5/30/2017.
 */
public class User {
    private String userName;
    private int followers;

    /**
     * The constructor for the User class, creates a user with a unique username and a count of followers
     * @param userName A unique string identifying the user
     * @param followers An int of the number of users following this user
     */
    public User(String userName, int followers) {
        this.userName = userName;
        this.followers = followers;
    }

    /**
     * @return Returns the User's unique user ID
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the User's unique user ID
     * @param userName Unique User ID
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return Returns the User's follower count
     */
    public int getFollowers() {
        return followers;
    }

    /**
     * Sets the User's follower count
     * @param followers
     */
    public void setFollowers(int followers) {
        this.followers = followers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return userName != null ? userName.equals(user.userName) : user.userName == null;
    }

    @Override
    public int hashCode() {
        return userName != null ? userName.hashCode() : 0;
    }
}
