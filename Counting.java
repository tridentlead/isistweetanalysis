import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by danie on 6/1/2017.
 */
public class Counting {

    /**
     * @param user The User for whom the query is
     * @param dataset The loaded dataset
     * @return The count of posts by a given user
     */
    public static int postCountOfUser(User user, HashMap<User, ArrayList<Tweet>> dataset) {
        return dataset.get(user) == null ? 0 : dataset.get(user).size();
    }

    /**
     * @param word The word to be searched for
     * @param dataset The loaded dataset
     * @return The count of posts containing a given word
     */
    public static int countWithWord(String word, HashMap<User, ArrayList<Tweet>> dataset) {
        return Finding.findWithWord(word, dataset).size();
    }

    /**
     * @param tweet The tweet to be analysed
     * @return A mapping of words in the tweet to the number of times they occur
     */
    public static HashMap<String, Double> wordOccurrences(Tweet tweet) {
        HashMap<String, Double> percentages = new HashMap<String, Double>();
        String[] words = tweet.getBodyAsStringArray();
        for (String word : words) {
            if (percentages.get(word) == null) {
                percentages.put(word, 0.00);
            }
            percentages.put(word, percentages.get(word) + 1);
        }
        return percentages;
    }

    /**
     *
     * @param dataset The loaded dataset
     * @return A mapping of word in the overall dataset to the number of times they occur
     */
    public static HashMap<String, Double> overallWordOccurrences(HashMap<User, ArrayList<Tweet>> dataset) {
        HashMap<String, Double> ret = new HashMap<String, Double>();
        for (User user : dataset.keySet()) {
            for (Tweet tweet : dataset.get(user)) {
                _mergeHashMapBySummation(ret, Counting.wordOccurrences(tweet));
            }
        }
        return ret;
    }

    /**
     * @param base The base map to be merged onto
     * @param addition The map to merge from
     * @return A list merging the values in the two maps, by summation if existing in both sets, or by addition if not
     */
    private static HashMap<String, Double> _mergeHashMapBySummation(HashMap<String, Double> base, HashMap<String, Double> addition) {
        for (String key : addition.keySet()) {
            if (base.get(key) == null) {
                base.put(key, addition.get(key));
            }
            base.put(key, addition.get(key) + base.get(key));
        }
        return base;
    }

    /**
     * @param occurrences A map of words to the number of times they occur
     * @return A map of words to the percentage likelyhood of their occurrence
     */
    public static HashMap<String, Double> occurrencesToPercentageOccurrence(HashMap<String, Double> occurrences) {
        int count = 0;
        for (String key : occurrences.keySet()) {
            count += occurrences.get(key);
        }
        for (String key : occurrences.keySet()) {
            occurrences.put(key, occurrences.get(key)/count*100);
        }
        return occurrences;
    }
}
